const express = require("express");
const router = express.Router();
const courseController = require("../controllers/courseControllers");
const auth = require("../auth");

//Route for creating a course
//refactor this route to implement user authentication for our admin when creating a course

router.post("/",auth.verify,(req,res)=>{

	const data ={
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	courseController.addCourse(req.body).then(resultFromController=>res.send(resultFromController))
});


module.exports = router;

/*
{
	"name": "HTML",
	"description": "Study HTML",
	"price": 2000
}
*/