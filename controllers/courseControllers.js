const Course = require("../models/Course");
const User = require("../models/User");

//Mini Activity
//Create a new course
/*
	Steps:
	1.Create a new Course object using the mongoose model and the information from the reqBody
		name
		description
		price
	2.Save the new User to the database
	3. Send a screenshot of 3 courses from your database

*/

// Worked without if else
/*module.exports.addCourse = (reqBody) => {
	let newCourse = new Course({
		name : reqBody.name,
		description : reqBody.description,
		price : reqBody.price
	});


	return newCourse.save().then((course, error) => {
		if (error) {
			return false;
		} else {
			return true;
		};
	});
};*/

// module.exports.addCourse = (reqBody) => {
// 	let newCourse = new Course({
// 		name : reqBody.name,
// 		description : reqBody.description,
// 		price : reqBody.price
// 	});

// 	return User.find({}).then(result=>{
// 	return newCourse.save().then((course, error) => {
// 		if (error) {
// 			return false;
// 		} else {
// 			return true;
// 		};
// 	});
// 	});
// };

module.exports.addCourse = (reqBody) => {
	let newCourse = new Course({
		name : reqBody.name,
		description : reqBody.description,
		price : reqBody.price
	});

	return User.find({}).then(result=>{
		if(result.isAdmin){
				return newCourse.save().then((course, error) => {
				if (error) {
					return false;
				} else {
					return true;
				};
			});
		}else{
			return false;
		}
	});
};

